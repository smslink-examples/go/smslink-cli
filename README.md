# ❔ A propos

Forked from [opt-nc/mobitag](https://github.com/opt-nc/mobitag)

Cette repo est un fork de la repo d'👉 [Adrien Sales](https://www.linkedin.com/in/adrien-sales/)👈 je cite :

> Cette repo est une **première expérimentation dont le but est de découvrir le
> language [`Go`](https://go.dev/)**, sur un cas concret car... c'est plus amusant
> et beaucoup plus motivant 🤓.

Cette expérimentation a donc pur but de créer un cli permettant d'envoyer des SMS via la plateforme SMSlink 
depuis le terminal.

![](media/smslink-cli.gif)


# 🔖 Ressources SMSlink

- [Site web officiel](https://smslink.nc)
- [Documentation de l'API](https://api.smslink.nc/api/documentation)

# 🔖 Ressources Originales

- [🥳 mobitag.nc... 25 ans plus tard, des sms en SaaS via API{GEE}](https://dev.to/optnc/mobitagnc-25-ans-plus-tard-des-sms-en-saas-via-apigee-2h9e)
- [📲 mobitag.nc for dummies](https://www.kaggle.com/code/optnouvellecaldonie/mobitag-nc-for-dummies)
- [⏱️ mobitag Go Hackathon 2024-06-22 week-end 🤓](https://dev.to/adriens/mobitag-go-hackathon-2024-06-22-week-end-2n16)
- [⏱️ mobitag Hackathon week-end du 2024-06-22 🤓](https://youtu.be/yVoMg7CXgaM)

# ✅ Prérequis

- [x] Tooling `Go` ([installer `Go`](https://go.dev/doc/install))
- [x] Un Jeton d'Accès Personnel, chargé dans l'environnement `SMSLINK_NC_PAT`

# 🚀 Getting started

## 🤓 `go install`

```shell
go install gitlab.com/smslink-examples/go/smslink-cli@latest
export PATH=$PATH:$(go env GOPATH)/bin
source ~/.bashrc

```

Puis : 

```sh
smslink -h

```

## ⚙️ Builder

```shell
go build smslink.go

```

# 🕹️ Essayer

```sh
./smslink -h

```

```sh
# Tester l'environnement
./smslink --dry-run

```
# 🥳 Envoyer un `SMS`

```sh
./smslink -to xxxxxx -message "Hello World : a SMSlink message from Go(lang) 😃"

```

# 📼 Builder la demo video

La video de demo est buildée avec [`charmbracelet/vhs`](https://github.com/charmbracelet/vhs):

```sh
vhs smslink-cli.tape

```
